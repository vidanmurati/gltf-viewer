# Rapport projet 

Sujet choisi : Many lights
Le viewer n'a pas de spécificités particulières

Concernant les difficultés rencontrées, je n'ai pas réussi à modéliser la spotlight correctement.
Faute de temps je n'ai pas pu travailler sur l'aspect "many" à savoir le calcul de plusieurs lumières pour chaque fragment.
J'aurais procédé en ajoutant un champ mode sur mes lights me donnant le type de light dans le shader puis en additionnant les résultats des calculs des différentes lights sur mon fragment.

Connaissances acquises : Comment fonctionne une spotlight / Comment fonctionne une point light