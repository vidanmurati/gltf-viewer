#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"
#include "utils/lights.hpp"

#include <stb_image_write.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());

  for (unsigned int i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(),
        model.buffers[i].data.data(), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  std::vector<GLuint> vertexArrayObjects;

  for (auto it = model.meshes.begin(); it < model.meshes.end(); it++) {
    const int vaoOffset = vertexArrayObjects.size();

    vertexArrayObjects.resize(vaoOffset + (*it).primitives.size());
    meshIndexToVaoRange.push_back(VaoRange{vaoOffset,
        (GLint)((*it).primitives.size())}); // Will be used during rendering

    glGenVertexArrays((*it).primitives.size(), &vertexArrayObjects[vaoOffset]);

    for (size_t primitiveIdx = 0; primitiveIdx < (*it).primitives.size();
         primitiveIdx++) {

      auto vaoPrimitive = vertexArrayObjects[vaoOffset + primitiveIdx];
      auto primitive = (*it).primitives[primitiveIdx];
      glBindVertexArray(vaoPrimitive);

      GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
      GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
      GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

      auto iterator = primitive.attributes.find("POSITION");
      if (iterator != end(primitive.attributes)) {
        // If "POSITION" has been found in the map
        // (*iterator).first is the key "POSITION", (*iterator).second is the
        // value, ie. the index of the accessor for this attribute

        // TODO get the correct
        // tinygltf::Accessor from
        // model.accessors
        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];

        // TODO get the correct
        // tinygltf::BufferView from
        // model.bufferViews. You need to
        // use the accessor
        const auto &bufferView = model.bufferViews[accessorIdx];

        // TODO get the index of the buffer used by
        // the bufferView (you need to use it)
        const auto bufferIdx = bufferView.buffer;

        // TODO get the correct buffer object from the buffer index
        const auto bufferObject = bufferObjects[bufferIdx];

        // TODO Enable the vertex attrib array corresponding to POSITION
        // with glEnableVertexAttribArray (you need to use
        // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
        // the cpp file)
        glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
        // TODO Bind the buffer object to GL_ARRAY_BUFFER
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

        // TODO Compute the total byte offset using
        // the accessor and the buffer view
        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

        // TODO Call glVertexAttribPointer with the correct arguments.
        // Remember size is obtained with accessor.type, type is obtained with
        // accessor.componentType. The stride is obtained in the bufferView,
        // normalized is always GL_FALSE, and pointer is the byteOffset (don't
        // forget the cast).

        glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      iterator = primitive.attributes.find("NORMAL");
      if (iterator != end(primitive.attributes)) {

        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessorIdx];
        const auto bufferIdx = bufferView.buffer;
        const auto bufferObject = bufferObjects[bufferIdx];

        glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

        glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      iterator = primitive.attributes.find("TEXCOORD_0");
      if (iterator != end(primitive.attributes)) {

        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessorIdx];
        const auto bufferIdx = bufferView.buffer;
        const auto bufferObject = bufferObjects[bufferIdx];

        glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
        glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

        glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
            accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
            (const GLvoid *)byteOffset);
      }

      if (primitive.indices >= 0) {
        const auto &accessor = model.accessors[primitive.indices];
        const auto &bufferView = model.bufferViews[primitive.indices];
        const auto bufferIdx = bufferView.buffer;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
      }
    }
  }
  glBindVertexArray(0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{
  // Texture object creation
  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0);
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (size_t i = 0; i < model.textures.size(); i++) {
    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0);             // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image

    const auto &sampler =
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);

    // fill the texture object with the data from the image
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{

  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
  // bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, argv[1]); // for
  // binary glTF(.glb)

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return false;
  }
  return ret;
}

int ViewerApplication::run()
{
  tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltfFile(model)) {
    return -1;
  }

  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto uModelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto uModelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto uNormalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uLightDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto uBaseColorTexture =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto uMetallicRoughnessTexture =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uMetallicFactor =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactor =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uEmissiveTexture =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uOcclusionTexture =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionStrength =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
  const auto uApplyOcclusion =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");
  const auto uConstantLight =
      glGetUniformLocation(glslProgram.glId(), "uConstantLight");
  const auto uQuadraticLight =
      glGetUniformLocation(glslProgram.glId(), "uQuadraticLight");
  const auto uLinearLight =
      glGetUniformLocation(glslProgram.glId(), "uLinearLight");
  const auto uLightPosition =
      glGetUniformLocation(glslProgram.glId(), "uLightPosition");
  const auto uLightCutOff =
      glGetUniformLocation(glslProgram.glId(), "uLightCutoff");
  const auto uLightMode =
      glGetUniformLocation(glslProgram.glId(), "uLightMode");

  // Bounding box calculation
  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  glm::vec3 bboxCenter = (bboxMin + bboxMax) * 0.5f;
  glm::vec3 bboxDiagonal = (bboxMax - bboxMin);
  glm::vec3 up = glm::vec3(0, 1, 0);

  // Build projection matrix
  auto maxDistance = glm::length(bboxDiagonal);
  if (maxDistance == 0)
    maxDistance = 100;
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;

  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {

    if (bboxMax.z == 0) {
      cameraController->setCamera(
          Camera{bboxCenter + 2.f * glm::cross(bboxDiagonal, up),
              bboxCenter + bboxDiagonal, glm::vec3(0, 1, 0)});
    } else {
      cameraController->setCamera(
          Camera{bboxCenter, bboxCenter + bboxDiagonal, glm::vec3(0, 1, 0)});
    }
  }

  // Load textures
  const auto textureObjects = createTextureObjects(model);

  GLuint whiteTexture = 0;
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  float white[] = {1, 1, 1, 1};
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  std::vector<GLuint> bufferObjects = createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();
  Light light;
  light.position = glm::vec3(1);
  light.direction = glm::vec3(1);
  light.ambient = glm::vec3(1);
  light.constant = 1.0f;
  light.linear = 0.09f;
  light.quadratic = 0.032f;
  light.cutOff = glm::cos(glm::radians(12.5f));

  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      if (uBaseColorTexture >= 0) {
        auto textureObject = whiteTexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];

          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uMetallicFactor >= 0) {
        glUniform1f(
            uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(
            uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (uMetallicRoughnessTexture >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                 .index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (uEmissiveTexture >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTexture, 2);
      }
      if (uOcclusionStrength >= 0) {
        glUniform1f(
            uOcclusionStrength, (float)material.occlusionTexture.strength);
      }
      if (uOcclusionTexture >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uOcclusionTexture, 3);
      }

    } else {
      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor, 1, 1, 1, 1);
      }

      if (uBaseColorTexture >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uMetallicFactor >= 0) {
        glUniform1f(uMetallicFactor, 1.f);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(uRoughnessFactor, 1.f);
      }
      if (uMetallicRoughnessTexture >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTexture >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTexture, 2);
      }
      if (uOcclusionStrength >= 0) {
        glUniform1f(uOcclusionStrength, 0.f);
      }
      if (uOcclusionTexture >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTexture, 3);
      }
    }
  };

  static bool lightFromCamera = false;
  static bool applyOcclusion = true;
  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();
    // Lights

    if (uLightDirectionLocation >= 0 && uLightPosition >= 0) {
      if (lightFromCamera) {
        glUniform3f(uLightDirectionLocation, 0, 0, 1);
        glUniform3f(
            uLightPosition, camera.eye()[0], camera.eye()[1], camera.eye()[2]);
      } else {
        const auto lightDirectionInViewSpace = glm::normalize(
            glm::vec3(viewMatrix * glm::vec4(light.direction, 0.)));

        glUniform3f(uLightDirectionLocation, lightDirectionInViewSpace[0],
            lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);

        glUniform3f(uLightPosition, light.position[0], light.position[1],
            light.position[2]);
      }
    }

    if (uLightIntensityLocation >= 0) {
      glUniform3f(uLightIntensityLocation, light.ambient[0], light.ambient[1],
          light.ambient[2]);
    }

    if (uApplyOcclusion >= 0) {
      glUniform1i(uApplyOcclusion, applyOcclusion);
    }

    // if PointLight
    if (uConstantLight >= 0)
      glUniform1f(uConstantLight, light.constant);

    if (uQuadraticLight >= 0)
      glUniform1f(uQuadraticLight, light.quadratic);

    if (uLinearLight >= 0)
      glUniform1f(uLinearLight, light.linear);

    // if Spotlight
    if (uLightCutOff >= 0)
      glUniform1f(uLightCutOff, light.cutOff);

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          auto node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if (node.mesh >= 0) {
            const auto mvMatrix = viewMatrix * modelMatrix; // modelViewMatrix
            const auto mvpMatrix = projMatrix * mvMatrix; // modelViewProjMatrix
            const auto normalMatrix = glm::transpose(glm::inverse(mvMatrix));

            glUniformMatrix4fv(uModelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(mvpMatrix));
            glUniformMatrix4fv(uModelViewMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(mvMatrix));
            glUniformMatrix4fv(uNormalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            const auto &mesh = model.meshes[node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[node.mesh];
            for (size_t i = 0; i < mesh.primitives.size(); i++) {
              const auto vao = vertexArrayObjects[vaoRange.begin + i];
              const auto primitive = mesh.primitives[i];
              bindMaterial(primitive.material);
              glBindVertexArray(vao);
              if (primitive.indices >= 0) {
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;

                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }

              for (auto childrenIdx : node.children) {
                drawNode(childrenIdx, modelMatrix);
              }
            }
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      auto nodes = model.scenes[model.defaultScene].nodes;
      for (unsigned int nodeIdx = 0; nodeIdx < nodes.size(); nodeIdx++) {
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  if (!m_OutputPath.empty()) {
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(),
        [&]() { drawScene(cameraController->getCamera()); });

    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0;
  }

  static float GUItheta = 0.0f;
  static float GUIphi = 0.0f;
  static glm::vec3 GUIlightColor(1.f, 1.f, 1.f);
  static float GUIintensity = 1.f;

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Lighting", ImGuiTreeNodeFlags_DefaultOpen)) {
        auto lightDirChanged =
            ImGui::SliderFloat("Theta", &GUItheta, 0.0f, 3.1415f, "%.3f") ||
            ImGui::SliderFloat("Phi", &GUIphi, 0.0f, 2.f * 3.1415f, "%.3f");
        if (lightDirChanged) {
          light.direction = glm::vec3(sin(GUItheta) * cos(GUIphi),
              cos(GUItheta), sin(GUItheta) * sin(GUIphi));
        }

        auto lightIntensityChanged =
            ImGui::ColorEdit3("Color", (float *)&GUIlightColor) ||
            ImGui::SliderFloat("Intensity", &GUIintensity, 0.0f, 5.f, "%.3f");
        if (lightIntensityChanged) {
          light.ambient = GUIintensity * GUIlightColor;
        }

        ImGui::Checkbox("Light from Camera", &lightFromCamera);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);

        static int lightMode = 0;
        const auto modeChanged =
            ImGui::RadioButton("Directional Light", &lightMode, 0) ||
            ImGui::RadioButton("Point Light", &lightMode, 1) ||
            ImGui::RadioButton("Spot Light", &lightMode, 2);
        if (modeChanged) {
          glUniform1i(uLightMode, lightMode);
        }
      }

      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }
        static int mode = 0;
        const auto modeChanged =
            ImGui::RadioButton("Trackball Camera", &mode, 0) ||
            ImGui::RadioButton("First Person Camera", &mode, 1);
        if (modeChanged) {
          auto currentCam = cameraController->getCamera();
          switch (mode) {
          case 0:
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
            break;
          case 1:
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
            break;
          }
          cameraController->setCamera(currentCam);
        }
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
